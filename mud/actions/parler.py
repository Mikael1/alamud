from .action import Action2
from mud.events import ParlerEvent

class ParlerAction(Action2):
    EVENT = ParlerEvent
    ACTION = "parler"
    RESOLVE_OBJECT = "resolve_for_operate"
